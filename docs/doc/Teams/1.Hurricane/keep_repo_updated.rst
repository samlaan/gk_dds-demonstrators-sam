.. _keep_repo_updated:

How-to keep your forked repo up-to-date
---------------------------------------

In order to keep our forked repository up to date with the original repository as well as with the repositories of each of the collaborators, we add upstream repositories from which we can merge the changes into our reporitory.

First run::

  $ git remote add upstream <url_to_repo>

.. note:: 

   In the above command ``upstream`` refers to the name so give it a significant name if you are going to add multiple remote repositories.

List all the remote repositories with:: 

  $ git remote -v

If the remote has been added succesfuly do the following to merge the changes into your own master branch:: 

  $ git fetch uptream 
  $ git checkout master
  $ git merge upstream/master

If you want to push the changes to your own remote repository do::

  $ git push origin master

Finally, in order to remove the ``upstream`` remote repository simply::

  $ git remote rm upstream

Links
"""""
* Keep forked repository current: https://dev.to/ranewallin/how-to-keep-your-forked-repository-current-38mn





