.. (C) ALbert Mietus, Sogeti, 2019

.. _XScope:

XScope
======
The **XScope** Demonstrator is kind of an “add-On” tool, to debug other :ref:`Demonstrators <Demonstrators>`. Several
variant of the XScope are foreseen: a basic version, and possible some “subclasses” to tailor for specific
Demonstrators.
|BR|
Possible a generic one will become available once.

.. demo:: XScope (base)
   :ID: XS_001
   :tags: XS
   :status: import

   The (base) XScope is like an “oscilloscope”, for DDS-messages. It a passive tool, and only listening to topics. It
   will frequently “collect” samples of the (configured, list of) Topics and store/print/show them.

   When the value of (a part of) a Topic in numeric, it should be able to draw them on a time-scale. A very-basic
   version this draw-feature is allowed to do “off-line”, by example via an Excel (export to csv and draw later in Excel).

   By design, this XScope-tool should extendable. The basic functionality should be in the base-XScope; an optional, flexible
   adaptation-layer (towards a future DDS-Demonstrator) can be used later -- by example to “find” the above mentioned
   numeric value in a Topic

   The “List of Topics” to monitor is preferable configurable; both in (eg. an) ini-file and on the commandline (without
   referring to a file).

