import cdds as dds
EVERY = 1000

class MessageBase:
    def __init__(self, val=None):
        self.val = val

    def gen_key(self):
        return type(self).__name__

    def __str__(self):                                                  # for debugging
        return "<%s key='%s': %s>" % (type(self).__name__, self.gen_key(),
                                      ", ".join("%s=%s" % (k,v) for (k,v) in self.__dict__.items()))

class IntMessage(MessageBase): pass


def send_data(w, val):
    m = IntMessage(val)
    w.write(m)

def process_from(listener, call, call2):
    i=0
    for s in listener.take(dds.all_samples()):
        if s[1].valid_data:
            val = s[0].val
            call(val)
            i+=1
        else:
            print("Unexpected:", s[0], s[1])
            call2(listener)
            return
    print("Received information from: [%s]" % (listener.flexy_topic.topic_name))
    return s[0], i


def demo_out_in(SendingTopic, ReceivingTopic):
    print("Sending on  :", SendingTopic);
    print("Receiving on:", ReceivingTopic)



M_OUT="Master_Out"
M_IN="Master_in"
